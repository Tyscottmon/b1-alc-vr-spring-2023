using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BlockColor
{
    Green,
    Red
}

public class Block : MonoBehaviour
{

public BlockColor color;

public GameObject brokenBlockLeft;
public GameObject brokenBlockRight;
public float brokenBlockForce;
public float brokenBlockTorque;
public float brokenBlockDestroyDelay;

void OnTriggerEnter (Collider other)
{
if(other.CompareTag("Red Saber"))
{
    if(color == BlockColor.Red && GameManager.instance.rightSwordTracker.velocity.magnitude >= GameManager.instance.swordHiteVelocityThreshold)
    {
    GameManager.instance.AddScore();
    }
    else
    {
       GameManager.instance.HitWrongBlock();
    }
    Hit();
}
    else if(other.CompareTag("Green Sword"))
    {
        if(color == BlockColor.Green && GameManager.instance.leftSwordTracker.velocity.magnitude >= GameManager.instance.swordHiteVelocityThreshold)
       { 
       GameManager.instance.AddScore();
       }
       else
       {
       GameManager.instance.HitWrongBlock();
       }
       Hit();
    }
    
    
}



public void Hit ()
{
 //enable broken pieces
 brokenBlockLeft.SetActive(true);
 brokenBlockRight.SetActive(true);
 
 //remove them as children
 brokenBlockLeft.transform.parent = null;
 brokenBlockRight.transform.parent = null;
 
 Rigidbody leftRig = brokenBlockLeft.GetComponent<Rigidbody>();
 Rigidbody rightRig = brokenBlockRight.GetComponent<Rigidbody>();
 
 leftRig.AddForce(-transform.right * brokenBlockForce, ForceMode.Impulse);
 rightRig.AddForce(transform.right * brokenBlockForce, ForceMode.Impulse);
 
 //add torque
 leftRig.AddTorque(-transform.forward *brokenBlockTorque, ForceMode.Impulse);
 rightRig.AddTorque(transform.forward *brokenBlockTorque, ForceMode.Impulse);
 
 Destroy(brokenBlockLeft, brokenBlockDestroyDelay);
 Destroy(brokenBlockRight, brokenBlockDestroyDelay);
 
 Destroy(gameObject);
}
}
