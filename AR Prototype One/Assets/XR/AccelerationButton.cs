using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AccelerationButton : MonoBehaviour
{
public void OnDown ()
{
Car.instance.doAccelerate = true;
}

public void OnUp ()
{
Car.instance.doAccelerate = false;
}
}
